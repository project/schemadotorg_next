Table of contents
-----------------

* Introduction
* Features
* Requirements
* Configuration
* References


Introduction
------------

The **Schema.org Blueprints Next.js module** assists with the integration of
Schema.org Blueprints with Next.js.


Features
--------

- Automatically creates a corresponding Next.js entity type for
  Schema.org types.
- When a Next.js entity type is deleted when its corresponding node type
  dependency is deleted.
- Moves Next.js preview to a dedicated details widget.


Requirements
------------

**[Next.js](https://www.drupal.org/project/next)**  
Next.js + Drupal for Incremental Static Regeneration and Preview mode.


Configuration
-------------

- Go to the Schema.org Next.js configuration page.  
  (/admin/config/schemadotorg/settings/general#edit-schemadotorg-next)
- Check/uncheck enhance the default Next.js preview.


References
----------

Next.js

- [Next.js](https://nextjs.org/)
- [Next.js for Drupal](https://next-drupal.org/)
- [Next.js | Drupal.org](https://www.drupal.org/project/next)
- [Next.js and Drupal go together like peanut butter and jelly!](https://www.chapterthree.com/blog/nextjs-and-drupal-go-together-like-peanut-butter-and-jelly)
- [How to progressively decouple your Drupal site with Next.js and JSON:API](https://www.chapterthree.com/blog/how-to-progressively-decouple-your-drupal-site-nextjs-and-jsonapi)
- [What's Next(js) for Drupal?](https://www.youtube.com/watch?v=QqSUk681msc)

Acquia CMS headless

- [Next.js on Acquia: Setting up Acquia CMS](https://dev.acquia.com/tutorial/nextjs-acquia-setting-acquia-cms)
- [Next.js for Acquia CMS](https://github.com/acquia/next-acms)
- [Use the Acquia CMS Headless Beta to Improve Headless Applications](https://www.bounteous.com/insights/2022/07/26/use-acquia-cms-headless-beta-improve-headless-applications/)

Pantheon decoupled architecture

- [Pantheon Front-End Sites](https://pantheon.io/docs/guides/decoupled-sites/)
- [Pantheon Decoupled Kit Next Drupal Starter](https://github.com/pantheon-systems/next-drupal-starter )
- [Decoupled Architectures on Pantheon with Front-End Sites](https://pantheon.io/features/decoupled-cms)
